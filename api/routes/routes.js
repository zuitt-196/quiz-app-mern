import { Router } from "express";
import * as  controller  from "../controller/controller.js";

const router = Router();

//  QUESTION ROUTER
router.get('/questions', controller.getQuestion);
router.post('/postQuestions', controller.postQuestion)
router.delete('/deleteQuestions', controller.deleteQuestion)


// QUESTION ROUTER
router.get('/result', controller.getAllResult);
router.post('/resultPost', controller.postAllResult);
router.delete('/resultDelete', controller.deleteAllResult);


export default router;