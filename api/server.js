import express  from 'express';
import env from 'dotenv';
import morgan from 'morgan'
import cors from 'cors';
import router from './routes/routes.js'
import connect from './connection.js';
env.config()
const app = express();
// APPLICATION PORT 
const PORT = process.env.PORT || 8000;


// BUILT IN MIDDLEWARE
app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());

// ROUTES API 
app.use('/api/quiz', router)



//  DATABASE CONNECTION FILE
connect().then(() =>{
    try {
        app.listen(PORT, () =>{
        console.log(`server is connected the post  http://localhost:${PORT}`);
})
    } catch (error) {
        console.log("Cannot Error to  the server");
    }
}).catch(err => console.log("Envalid Database connection"));

