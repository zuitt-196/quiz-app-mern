import { answerData, mockData } from "../database/data.js";
import Questions from "../models/QuestiosModel.js";
import Result from "../models/ResusltModel.js";

 

// GET ALL QUESION COLTRLLER
export const getQuestion = async (req, res) => {
    try {
        const question = await  Questions.find();
        res.status(200).json(question);
    } catch (error) {
        res.status(400).json({error})
    }

}

// POST QUESION COLTRLLER 
export const postQuestion = async (req, res) => {
    try {
            const question = await Questions.insertMany({question: mockData, answers:answerData})
            res.status(200).json(question);
    } catch (error) {
        res.status(400).json({error})
    }

}

// DELETE QUESION COLTRLLER 
export const deleteQuestion = async (req, res) => {
    try {
             await Questions.deleteMany();
             res.status(200).json({msg:"Question Delete Sucessfully"})

             
    } catch (error) {
        res.status(400).json({error})
    }
}


//GET ALL RESULT 
export const getAllResult = async (req, res) => {
    try {
        const result = await Result.find();
        res.status(200).json(result);

    } catch (error) {
        res.status(400).json({error});
    }
}

//POST ALL RESULT 
export const postAllResult = async (req, res) => {
    console.log(req.body);
    try {
        const {username, result, attempts, points,  achived}=req.body;
        if(!username && !result) throw new Error("Data not provided");
        
        const dataResult  = await Result.create({username, result, attempts, points,  achived});
        res.status(200).json(dataResult);

    } catch (error) {
        res.status(400).json(error)
    }
}

// DELETE ALL RESULT 
export const deleteAllResult = async (req, res) => {
    try {
        await Result.deleteMany();
        res.status(200).json({msg:"Result has beee Delelete Succesfully"})
    } catch (error) {
        res.json({error})
    }
}